@if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
     <button type="button" class="close" data-dismiss="alert">x</button>
     {{$message}}
   </div>
  @endif
  @if(count($errors->all()))
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
@endif

<div class="row">
   <div class="col-lg-6">
      <div class="form-group">
        {{Form::label('title', 'Enter title')}}
        {{Form::text('record[title]', '', ['class' => 'form-control', 'placeholder'=>'Enter title','id'=>'title','required'=>'required'])}}
      </div>
      <!-- <div class="form-group">
        {{Form::label('slug', 'Enter slug')}}
        {{Form::text('record[slug]','', ['class'=>'form-control', 'placeholder'=>'Enter slug', 'id'=>'slug'])}}
      </div> -->
      @php
        $language = [
          'english' => 'English',
          'hindi' => 'Hindi'
        ]
      @endphp
      <div class="form-group">
        {{Form::label('language', 'Select Language')}}
        {{Form::select('record[language]', $language,'0', ['class' => 'form-control', 'id'=>'language'])}}
      </div>
      <div class="form-group">
        {{ Form::label('excerpt', 'Enter short description') }}
        {{ Form::textarea('record[excerpt]', '', ['class' => 'form-control','id'=>'excerpt', 'placeholder'=>'Enter short description','rows'=>'4', 'col'=>'3','required'=>'required']) }}
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group">
        {{Form::label('cid', 'Select Category')}}
        {{Form::select('record[cid]', $parentArr,'0', ['class' => 'form-control', 'id'=>'cid','required'=>'required'])}}
      </div>
      <div class="form-group">
          {{Form::label('tid', 'Select Tag')}}
          {{Form::select('tid[]', $tagsArr,'0', ['class' => 'form-control selectpicker', 'id'=>'tid','required'=>'required', 'multiple'])}}
      </div>
       <div class="form-group">
        {{Form::label('image', 'Choose image')}}
        <div class="">
            @if(empty($image))
              <img src="{{ url('top.jpg') }}" class="add_media" id="show_image" style="width:120px;height:120px;"/>
              <input type="hidden" name="record[image]" value="" id="image">
            @else
              <img src="{{ url('storage/media/'. $image) }}" class="add_media" id="show_image" style="width:120px;height:120px;"/>
              <input type="hidden" name="record[image]" value="{{ $m_id }}" id="image">
            @endif
            
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        {{Form::label('description', 'Enter description')}}
        {{Form::textarea('record[description]', '', ['class' => 'form-control editor', 'placeholder'=>'Enter description', 'id'=>'description'])}}
      </div>
    </div>
    <h6 class="col-12"><p class="bg-primary text-white p-4 font-weight-bold">Meta Info</p></h6>
    <div class="col-lg-6">
       <div class="form-group">
         {{Form::label('seo_title', 'Title')}}
         {{Form::text('record[seo_title]', '', ['class' => 'form-control', 'placeholder'=>'Enter title', 'id'=>'seo_title'])}}
       </div>
     </div>
     <div class="col-lg-6">
       <div class="form-group">
         {{Form::label('seo_keywords', 'Keywords')}}
         {{Form::text('record[seo_keywords]','', ['class'=>'form-control', 'placeholder'=>'write...', 'id'=>'seo_keywords'])}}
       </div>
     </div>
     <div class="col-md-12">
       <div class="form-group">
         {{Form::label('seo_description', 'Description')}}
         {{Form::textarea('record[seo_description]', '', ['class' => 'form-control', 'placeholder'=>'write...','rows'=>'4', 'col'=>'3', 'id'=>'seo_description'])}}
       </div>
     </div>
  </div>
