@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

<div class="row">
@php
$typeArr =[
  'credit' => 'Credit',
  'debit'  =>  'Debit'
];
@endphp
<div class="col-lg-6"> 
    <div class="form-group">
      {{Form::label('user_id', 'Select User')}}
      {{Form::select('user_id', $userArr, '', ['class' => 'form-control','id'=>'user_id'])}}
    </div>  
  </div>
  <div class="col-lg-6"> 
    <div class="form-group">
      {{Form::label('type', 'Select Type')}}
      {{Form::select('type', $typeArr, '', ['class' => 'form-control','id'=>'type'])}}
    </div>  
  </div>
  <div class="col-lg-12">
    <div class="form-group">
      {{Form::label('amount', 'Enter amount')}}
      {{Form::text('amount', '', ['class' => 'form-control', 'placeholder'=>'Enter amount','id'=>'title','required'=>'required'])}}
    </div>    
  </div>
  
  <div class="col-lg-12">
    <div class="form-group">
      {{Form::label('remarks', 'Remarks')}}
      {{Form::textarea('remarks', '', ['class' => 'form-control', 'placeholder'=>'Enter Remarks', 'id'=>'remarks'])}}
    </div>
  </div>
  
</div>