@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<input type="hidden" name="type" value="{{ request('g_type') }}">
<div class="row">
  <div class="col-lg-2">
    <div class="form-group">

      {{Form::label('Game start')}}
      {{Form::time('start_game', $setting->start_game , ['class' => 'form-control', 'placeholder'=>'','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-2">
    <div class="form-group">

      {{Form::label('Game end')}}
      {{Form::time('stop_game', $setting->stop_game, ['class' => 'form-control', 'placeholder'=>'','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-3">
    <div class="form-group">
      {{Form::label('time')}}
      {{Form::number('time', $time, ['class' => 'form-control', 'placeholder'=>'Time in minutes','required'=>'required'])}}
    </div>
  </div>
    @php
    $typeArr = [
    $g_type => $g_type,
    ];
    $statusArr = [
    'low' => 'Low',
    'medium' => 'Medium',
    'high' => 'High'
    ];
    @endphp
  <div class="col-lg-2">
    <div class="form-group">
      {{Form::label('Game status')}}
      {{Form::select('status', $statusArr, '', ['class' => 'form-control','id'=>'status'])}}
    </div>
  </div>

</div>
</div>
