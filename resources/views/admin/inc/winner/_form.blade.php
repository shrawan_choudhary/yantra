@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif


  @foreach($timeslot as $t)
  <div class="row mb-3">
  <div class="col-lg-6"> 
    <div class="form-group">
      <!-- {{Form::label('timeslote_id', 'Select User')}} -->
      
      
      {{ $t->time }}
     
      
    </div>  
  </div>
  <div class="col-lg-6">
  {{Form::select('game['.$t->id.']', $gameArr[$t->id], $t->game_id, ['class' => 'form-control','id'=>'game_id', 'disabled' => $t->is_switch != '1'])}}
  </div>
  </div>
  @endforeach
<script>
var today = new Date().toISOString().split('T')[0];
document.getElementsByName("setTodaysDate")[0].setAttribute('min', today);
</script>
<script>
    function cityChangedTrigger () {
      
        let queryString = "{{ route('timeslote_schedule.create') }}";  // get url parameters        
        document.location = queryString+ "?type=" + document.getElementById("type").value; // refresh the page with new url
        
    }
</script>