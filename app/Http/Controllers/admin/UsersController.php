<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Model\Role;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = User::with('role')->get();
        // dd($lists);
        $page  = 'user.list';
        $title = 'User list';
        $data  = compact('lists','page','title');
        return view('admin.layout',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $page  = "user.add";
        $title = "User Add";
        $roles = Role::get();
        $roleArr = [
            '' => 'Select Role'
        ];
        foreach($roles as $r){
            $roleArr[$r->id] = $r->name;
        }
        
        $data  = compact('roleArr', 'page', 'title', 'request');
        return view('admin.layout',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'login'        => 'required',
            'password'     => 'required',
            'name'         => 'required',
            'mobile'       => 'required|unique:users',
            'email'        => 'required|unique:users',
            'role_id'      => 'required'
        ];
        

        $request->validate($rules);
        $input = $request->all();
        $input['password'] =  Hash::make($request->password);
        
        $obj = new User($input);

        $obj->save();

        return redirect(url('admin/user/'))->with('success', 'Success! New record has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user)
    {
        $edit = User::findOrFail($user->id);
        $request->replace($edit->toArray());       
        $request->flash();
        $roles = Role::get();
        $roleArr = [
            '' => 'Select Role'
        ];
        foreach($roles as $r){
            $roleArr[$r->id] = $r->name;
        }
        $page  = 'user.edit';
        $title = 'User Edit';
        $data  = compact('page', 'title','edit','request','roleArr');

        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'login'        => 'required',
            'password'     => 'required',
            'name'         => 'required',
            'email'        => 'required|unique:users,email,'.$user->id,
            'mobile'       => 'required|unique:users,mobile,'.$user->id,
            'role_id'      => 'required'
        ];
        $messages = [
            'login.required'     => 'Please Enter Login.',
            'password.required'  => 'Please Enter Password.',
            'name.required'      => 'Please Enter Name.',
            'email.required'     => 'Please Enter Email.',
        ];
        $request->validate($rules);
        $obj =  User::findOrFail($user->id);
        $input = $request->all();
        $input['password'] =  Hash::make($request->password);
        
        $obj->update($input);

        return redirect(url('admin/user/'))->with('success', 'Success!.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->sub_chk;
        // dd($ids);
        User::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }
}
