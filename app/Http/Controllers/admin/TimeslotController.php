<?php

namespace App\Http\Controllers\admin;

use App\Game;
use App\Http\Controllers\Controller;
use App\Model\Timeslot;
use App\Model\Setting;
use App\Model\Winner;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Facade\FlareClient\Time\Time;

class TimeslotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($request);
        $setting = Setting::findOrFail(1);
        $lists = Timeslot::where('type', $request->g_type)->get();

        //     $edit = Timeslot::where('type',$request->type)->get();
        //     foreach($edit as $e){
        //     $edit->staus = $request->status;
        //     $edit
        // }
        // $edit = \DB::table('timeslotes')
        // ->where('type','=',$request->type)
        // ->update('status', $request->status);
        $timeslot = Timeslot::orderBy('id', 'DESC')->get();
        if (count($timeslot) >= 2) {
            $first = $timeslot[1]->time;
            $second = $timeslot[2]->time;
            $start = strtotime($second);
            $end = strtotime($first);
            $mins = ($end - $start) / 60;
            $time = $mins;
        } else {
            $time = '10';
        }

        $g_type = $request->g_type;

        $page  = 'timeslot.list';
        $title = 'Timeslote List';
        $data  = compact('page', 'title', 'lists', 'setting', 'time', 'g_type');

        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response 
     */
    public function store(Request $request)
    {

        $rules = [
            'time'       =>  'required',
            'type'       =>  'required',
        ];
        $request->validate($rules);
        // $input = $request->all();
        // dd($request->all());
        $setting = Setting::findOrFail(1);
        $setting->start_game = $request->start_game;
        $setting->stop_game  = $request->stop_game;
        $setting->update();
        Timeslot::where('type', $request->type)->delete();
        Timeslot::where('type', NULL)->delete();
        $period = new CarbonPeriod($setting->start_game, $request->time . ' minutes', $setting->stop_game); // for create use 24 hours format later change format 
        // $slots = [];
        foreach ($period as $item) {
            $obj = new Timeslot();
            $obj->time = $item->format("H:i:s");
            $obj->type = $request->type;
            $obj->status = $request->status;
            $obj->is_switch = 0;
            $obj->save();
        }


        return redirect()->back()->with('success', 'Success! New record has been added.');
    }

    public function editstatus(Request $request)
    {
        $rules = [
            'type'       =>  'required',
            'status'     => 'required'
        ];
        $request->validate($rules);
        Timeslot::where('type', $request->type)->update(['status' => $request->status]);
        return redirect()->back()->with('success', 'Success! Status has been changed.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Timeslot  $timeslot
     * @return \Illuminate\Http\Response
     */
    public function show(Timeslot $timeslot)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Timeslot  $timeslot
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Timeslot $timeslot)
    {
        $edit = Timeslot::findOrFail($timeslot->id);
        $request->replace($edit->toArray());
        $request->flash();
        $page  = 'timeslot.edit';
        $title = 'Timeslote Edit';
        $data  = compact('page', 'title', 'edit');

        // return data to view
        return view('admin.layout', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Timeslot  $timeslot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Timeslot $timeslot)
    {
        $rules = [
            'time'       =>  'required',
        ];

        $request->validate($rules);
        $obj =  Timeslot::findOrFail($timeslot->id);
        $input = $request->all();


        $obj->update($input);

        return redirect()->back()->with('success', 'Success!.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Timeslot  $timeslot
     * @return \Illuminate\Http\Response
     */
    public function destroy(Timeslot $timeslot)
    {
        $timeslot->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->sub_chk;
        // dd($ids);
        Timeslot::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }

    public function change_status(Request $request, Timeslot $id)
    {

        $id->update([$request->field => $request->is_switch]);
        return redirect()->back()->with('success', 'Status change successfully.');
    }

    public function editswitch(Request $request)
    {
        if ($request->switchtype == 'all') {
            $timeslots =  Timeslot::get();
        }
        if ($request->switchtype == 'custom') {
            $ids = $request->sub_chk;
            $timeslots =  Timeslot::whereIn('id', $ids)->get();
        }
        foreach ($timeslots as $t) {
            $t->update(['is_switch' => $request->switch]);
        }
        return redirect()->back()->with('success', 'Switch change successfully.');
    }
}
