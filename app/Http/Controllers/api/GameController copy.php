<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Game;
use App\Model\Setting;
use App\Model\Timeslot;
use App\Model\Wallet;
use App\Model\Win;
use App\Model\Winner;
use App\Placepoint;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GameController extends Controller
{
    protected function game_script($type)
    {
        $datetime = time();

        // $timeslots = Timeslot::where('type', $type)->where('is_switch', 0)->orderBy('time', 'ASC')->get();
        $timeslots = Timeslot::where('type', $type)->orderBy('time', 'ASC')->get();
        $f_time = $next_date = $next_game_time = "";
        $finish = false;
        $is_next_date = false;
        $total_timeslots = count($timeslots->toArray());
        
        // if($total_timeslots == 0) {
        //     $next_date = date("Y-m-d", strtotime("+1 day"));
        //     $all_timeslots = Timeslot::where('type', $type)->orderBy('time', 'ASC')->get();
            
        //     $next_game_time = $all_timeslots[0]->time;
        // }

        foreach ($timeslots as $key => $tslot) {
            $time2 = strtotime(date("Y-m-d") . $tslot->time);
            
            if(!$finish) {
                if ($time2 > $datetime) {
                    $next_date = date("Y-m-d");
                    $next_game_time = $timeslots[$key]->time;

                    $finish = true;
                } else if($key == $total_timeslots - 1) {
                    $next_date = date("Y-m-d", strtotime("+1 day"));
                    $next_game_time = $timeslots[0]->time;
                    $is_next_date = true;

                    $finish = true;
                }
            }
        }
        $game_total_point = Placepoint::where('timeslot_id', $next_game_time)->where('date', $next_date)->where('user_id', auth()->user()->id)->sum('point');
        $games = Game::where('type', $type)->get();
        foreach ($games as $g) {
            $total_place = Placepoint::where('timeslot_id', $next_game_time)->where('date', $next_date)->where('game_id', $g->id)->sum('point');

            $g->total_point = $total_place;
        }
        
        $timeslots = Timeslot::where('type', $type)->where('is_switch', 0)->orderBy('time', 'ASC')->get();

        // dd($timeslots->toArray());

        $f_time = $f_date = $timeslot_time = "";
        $finish = false;

        foreach ($timeslots as $key => $tslot) {
            $time2 = strtotime($tslot->time);

            if ($key == 0 && $time2 > $datetime) {
                $f_date = date("Y-m-d", strtotime("-1 day"));
                $f_time = $timeslots[count($timeslots) - 1]->id;
                $timeslot_time = $timeslots[count($timeslots) - 1]->time;
                $finish = true;
            } else if (!$finish) {
                $f_date = date("Y-m-d");

                if ($time2 < $datetime) {
                    $f_time = $timeslots[$key]->id;
                    $timeslot_time = $timeslots[$key]->time;
                }
            }
        }


        $timeslote_status = Timeslot::where('time', $timeslot_time)->first();

        $place_point_count = Placepoint::where('timeslot_id', $timeslot_time)->where('date', $f_date)->count();

        $wins = Winner::where('type', $type)->where('date', $f_date)->where('timeslot_id', $timeslot_time)->first();



        if (empty($wins->id)) {

            // if ($place_point_count >= 1) {
                // if ($timeslote_status->status == 'high') {
                //     $game = \DB::table('place_points')
                //         ->where('timeslot_id', '=', $timeslot_time)
                //         ->where('date', '=', $f_date)
                //         ->selectRaw("SUM(point) as total_point")
                //         ->select('place_points.game_id')
                //         ->groupBy('game_id')
                //         ->first();
                // }
                // if ($timeslote_status->status == 'low') {

                    // $game = \DB::table('game AS g')
                    //     ->leftJoin('place_points AS pp', 'g.id', 'pp.game_id')
                    //     ->where('pp.timeslot_id', '=', $timeslot_time)
                    //     ->where('pp.date', '=', $f_date)
                    //     ->selectRaw("SUM(point) as total_point")
                    //     ->select('place_points.game_id')
                    //     ->groupBy('game_id')
                    //     ->orderBy(\DB::raw('SUM(point)'))
                    //     ->first();
                    $game = DB::select("SELECT g.id, IFNULL(pp.totalCount, 0) AS pp_counts FROM yg_game AS g LEFT JOIN (SELECT game_id, SUM(point) AS totalCount FROM yg_place_points WHERE timeslot_id = '".$timeslot_time."' AND `date` = '".$f_date."' GROUP BY id) AS pp ON g.id = pp.game_id WHERE g.type = '" . $type . "' ORDER BY pp_counts ASC LIMIT 1");
                    $game = $game[0];
                // }
                // if ($timeslote_status->status == 'medium') {

                    // $game_status =  \DB::table('place_points')
                    //     ->where('timeslot_id', '=', $timeslot_time)
                    //     ->where('date', '=', $f_date)
                    //     ->selectRaw("SUM(point) as total_point")
                    //     ->select('place_points.game_id')
                    //     ->groupBy('game_id')
                    //     ->orderBy(\DB::raw('SUM(point)'))
                    //     ->get();
                    // $game_id = ceil($game_status->count() / 2);

                    // $game = $game_status[$game_id - 1];

                    // dd($game);


                // }

                $wins = new Winner();
                $wins->timeslot_id = $timeslot_time;
                $wins->date        = $f_date;
                // $wins->game_id     = @$place_game->id;
                $wins->game_id     = $game->id;
                $wins->type        = $type;
                $wins->save();
            // } else {
            //     $game = Game::where('type', $type)->orderBy(\DB::raw('RAND()'))->first();

            //     $wins = new Winner();
            //     $wins->timeslot_id = $timeslot_time;
            //     $wins->date        = $f_date;
            //     // $wins->game_id     = @$place_game->id;
            //     $wins->game_id     = $game->id;
            //     $wins->type        = $type;
            //     $wins->save();
            // }
        }


        $place_point = Placepoint::where('timeslot_id', $timeslot_time)->where('game_id', $wins->game_id)->where('date', $f_date)->get();

        $setting = Setting::findOrFail(1);

        foreach ($place_point as $p) {
            $point = Placepoint::where('timeslot_id', $timeslot_time)->where('game_id', $wins->game_id)->where('user_id', $p->user_id)->where('date', $f_date)->sum('point');

            $point *= $setting->return_point;
            
            $ab = Win::where('user_id', $p->user_id)->where('timeslot_id', $p->timeslot_id)->where('game_id', $p->game_id)->where('date', $f_date)->get();

            if ($ab->count() == 0) {
                $win_data = new Win();
                $win_data->user_id = $p->user_id;
                $win_data->game_id = $p->game_id;
                $win_data->timeslot_id = $p->timeslot_id;
                $win_data->point = $point;
                $win_data->date = $f_date;
                $win_data->save();

                $add_wallet = new Wallet();
                $add_wallet->user_id = $p->user_id;
                $add_wallet->amount  = $point;
                $add_wallet->remarks = ' Win point ' . $point;
                $add_wallet->type    = "credit";
                $add_wallet->save();
            }
        }

        $ndate = $next_date . ' ' . $next_game_time;
        $cdate = date('Y-m-d H:i:s');
        $datetime1 = new DateTime($ndate);
        $datetime2 = new DateTime($cdate);
        $diff = $datetime1->diff($datetime2);

        $re = [
            'games'             => $games,
            'win_game'          => $wins,
            'game_total_point'  => $game_total_point,
            'current_time'      => date('Y-m-d H:i:s'),
            'next_game_time'    => $ndate,
            'time_difference'   => [
                'seconds'   => $diff->s,
                'minutes'   => $diff->i,
                'hours'     => $diff->h,
                'total_seconds' => (strtotime($ndate) - time()),
            ],
            'is_next_date' => $is_next_date
        ];

        return $re;
    }

    public function cron_game()
    {
        $types = ['yantra', 'city'];
        $datetime = time();
        $data = [];
        foreach($types as $type) {
            $data[$type] = $this->game_script($type);
        }
        $re = [
            'success' => true,
            'data'    => $data
        ];
        return response()->json($re);
    }
    public function game($type)
    {
        $re = $this->game_script($type);

        $totalCredits = Wallet::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();
        $balance = 0;
        foreach ($totalCredits as $list) {
            if ($list->type == 'credit') {
                $balance += $list->amount;
            } else {
                $balance -= $list->amount;
            }
        }

        $re['wallet_amt'] = $balance;

        return response()->json($re);
    }
    public function win_user(Request $request)
    {
        $re = Win::where('user_id', $request->user_id)->get();

        return response()->json($re);
    }
    public function userwinHistory(Request $request)
    {
        $request->validate([
            'date'  => 'required'
        ]);

        $datetime = time();

        // $games = Game::get();

        $timeslots = Timeslot::orderBy('time')->get();

        $f_time = $f_date = $timeslot_time  = "";
        $finish = false;

        foreach ($timeslots as $key => $tslot) {
            $time2 = strtotime(date("Y-m-d") . $tslot->time);

            // print_r([$datetime, $time2, $tslot->time]);
            if ($key == count($timeslots) && $time2 < $datetime) {
                $f_date = date("Y-m-d", strtotime("+1 day"));
                // $f_time = $timeslots[0]->id;
                $timeslot_time = $timeslots[0]->time;
                $finish = true;
            } else if ($time2 > $datetime && !$finish) {
                $f_date = date("Y-m-d");
                // $f_time = $timeslots[$key]->id;
                $timeslot_time = $timeslots[$key]->time;
                $finish = true;
            }
        }
        $f_date = $request->date;
        $wins = Win::where('user_id', auth()->user()->id)->where('date', $f_date)->get();
        
        $user = Placepoint::where('timeslot_id', '=', $timeslot_time)->where('date', '=', $f_date)->select('id')->get()->toArray();

        $place_point = PlacePoint::whereNotIn('id', $user)->where('user_id', auth()->user()->id)->where('date', $f_date)->orderBy('id', 'DESC')->get();



        foreach ($place_point as $key => $p) {
            foreach ($wins as $w) {
                if ($p->game_id == $w->game_id && $p->timeslot_id == $w->timeslot_id) {
                    $place_point[$key]->point   = $w->point;
                    $place_point[$key]->type = 'win';
                } else {
                    $place_point[$key]->type = 'lost';
                }
            }
        }


        $re = [
            'Win' => $place_point
        ];

        return response()->json($re);
    }

    public function winHistory(Request $request)
    {
        $request->validate([
            'type'  => 'required',
            'date'  => 'required'
        ]);

        $win_history = DB::table('win')
            ->groupBy('date')
            ->pluck('date');

        $response = [
            "data"  => []
        ];
        // $i = 0;
        // foreach($win_history as $date) {
        $date = $request->date;
        $winns = DB::table('winner AS win')
            ->select('game.name', 'game.type', 'game.image', 'win.timeslot_id AS timeslot')
            ->leftjoin('game', 'win.game_id', 'game.id')
            ->where('date', $date)
            ->where('game.type', request('type'))
            ->orderBy('timeslot', 'DESC')
            ->get();

        if ($winns->count() > 0) {
            // $response[$i]['date'] = $date;
            $response['data'] = $winns;
            // $i++;
        }
        // }


        $re = [
            'win_user' => $response,

        ];



        return response()->json($response);
    }

    public function userWallet(Request $request)
    {
        $request->validate([
            'date'  => 'required'
        ]);

        $balance = 0;
        $totalCredits2 = Wallet::where('user_id', auth()->user()->id)->whereRaw('DATE(created_at) = ?', $request->date)->orderBy('id', 'DESC')->get();

        $totalCredits = Wallet::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();

        foreach ($totalCredits as $list) {
            if ($list->type == 'credit') {
                $balance += $list->amount;
            } else {
                $balance -= $list->amount;
            }
        }


        $re = [
            'place_point' => $totalCredits2,
            'total' => $balance,
        ];
        return response()->json($re);
    }
    public function userPlacepoint(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required'
        ]);
        if ($validator->fails()) {
            $data = array();
            $data['status'] = 'failed';
            $data['data'] = $validator->errors();
            $data['msg'] = 'Invalid Perameters';

            return response()->json($data);
        }
        $timeslots = Timeslot::where('type', $request->type)->orderBy('time')->get();
        $datetime = time();
        $f_time = $next_date = $next_game_time = "";
        $finish = false;



        foreach ($timeslots as $key => $tslot) {
            $time2 = strtotime(date("Y-m-d") . $tslot->time);

            // print_r([$datetime, $time2, $tslot->time]);
            if ($key == count($timeslots) && $time2 < $datetime) {
                $next_date = date("Y-m-d", strtotime("+1 day"));
                $f_time = $timeslots[0]->id;
                $next_game_time = $timeslots[0]->time;
                $finish = true;
            } else if ($time2 > $datetime && !$finish) {
                $next_date = date("Y-m-d");
                $f_time = $timeslots[$key]->id;
                $next_game_time = $timeslots[$key]->time;
                $finish = true;
            }
        }


        $games = Game::where('type', $request->type)->get();

        foreach ($games as $g) {
            $game = Placepoint::where('timeslot_id', $next_game_time)->where('date', $next_date)->where('user_id', auth()->user()->id)->where('game_id', $g->id)->select('point')->get();
            $g->game = $game;
            $point = 0;
            foreach ($game as $g_name) {
                $point = $point + $g_name->point;
            }

            $g->total_point = $point;
        }

        $data = [
            'game' => $games,
        ];
        return response()->json($data);
    }
}
