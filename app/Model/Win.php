<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Win extends Model
{
    protected $table   = "win";
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'date:F d, Y',
        'updated_at' => 'date:F d, Y',
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function game()
    {
        return $this->hasOne('App\Game', 'id', 'game_id');
    }
    

    protected $appends = ['game_name', 'user_name']; 

    public function getGameNameAttribute()
    {
        return $this->game->name;
    }

    public function getUserNameAttribute()
    {
        return $this->user->name;
    }

   
    
    
}
