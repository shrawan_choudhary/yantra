<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    //
    protected $table   = "winner";
    protected $guarded = []; 

    protected $appends = ['game_name']; 

    public function getGameNameAttribute()
    {
        return $this->game ? $this->game->name : null;
    }

    public function game()
    {
        return $this->belongsTo('App\Game');
    }
    public function timeslot()
    {
        return $this->hasOne('App\Model\Timeslot', 'id', 'timeslot_id');
    }
}
