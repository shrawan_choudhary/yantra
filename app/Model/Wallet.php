<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table    = "wallet_credit";
    protected $guarded  =  [];

    

    public function user(){
      return $this->belongsTo('App\User'::class);
    }
    public function game(){
      return $this->hasOne('App\Game', 'id', 'game_id');
    }

    public static function get_wallet_amt($user_id) {
      $totalCredit = self::where('user_id', $user_id)->where('type', 'credit')->sum('amount');
      $totalDebit = self::where('user_id', $user_id)->where('type', 'debit')->sum('amount');

      return $totalCredit - $totalDebit;
    }
}
